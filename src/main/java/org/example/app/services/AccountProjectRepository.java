package org.example.app.services;

import java.util.List;

public interface AccountProjectRepository<T> {

    List<T> retrieveAll();

    void createAccount(T loginForm);

    boolean correctAccount(String username, String password);
}
