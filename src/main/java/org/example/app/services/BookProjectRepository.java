package org.example.app.services;

import java.util.List;

public interface BookProjectRepository<T> {
    List<T> retrieveAll();

    void store(T book);

    boolean removeItemById(Integer bookIdToRemove);

    boolean removeItemByAuthor(String bookAuthorToRemove);

    boolean removeItemByTitle(String bookTitleToRemove);

    boolean removeItemBySize(Integer bookSizeToRemove);

    boolean totalRemoving(Integer id, String author, String title, Integer size);

    List<T> filter(Integer id, String author, String title, Integer size);

//    List<T> filter(String filterMethod);
}
