package org.example.app.services;

import org.apache.log4j.Logger;
import org.example.web.dto.Book;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

@Repository
public class BookRepository implements BookProjectRepository<Book> {

    private final Logger logger = Logger.getLogger(BookRepository.class);
    private final List<Book> repo = new ArrayList<>();

    @Override
    public List<Book> retrieveAll() {
        return new ArrayList<>(repo);
    }

    @Override
    public void store(Book book) {

        book.setId(book.hashCode());
        logger.info("store new book " + book);
        repo.add(book);
    }

    @Override
    public boolean removeItemById(Integer bookIdToRemove) {

        if (bookIdToRemove == 0) {
            return false;
        }
        for (Book book : retrieveAll()) {
            if (book.getId().equals(bookIdToRemove)) {
                logger.info("Remove book completed " + book);
                return repo.remove(book);
            }
        }
        return false;
    }

    @Override
    public boolean removeItemByAuthor(String bookAuthorToRemove) {

        boolean wasRemoving = false;

        for (Book book : retrieveAll()) {
            if (book.getAuthor().equals(bookAuthorToRemove)) {
                logger.info("Remove book completed " + book);
                if (repo.remove(book)) {
                    wasRemoving = true;
                }
            }
        }
        return wasRemoving;
    }

    @Override
    public boolean removeItemByTitle(String bookTitleToRemove) {

        boolean wasRemoving = false;

        for (Book book : retrieveAll()) {
            if (book.getTitle().equals(bookTitleToRemove)) {
                logger.info("Remove book completed " + book);
                if (repo.remove(book)) {
                    wasRemoving = true;
                }
            }
        }
        return wasRemoving;
    }

    @Override
    public boolean removeItemBySize(Integer bookSizeToRemove) {

        boolean wasRemoving = false;

        for (Book book : retrieveAll()) {
            if (book.getSize() != null && book.getSize().equals(bookSizeToRemove)) {
                logger.info("Remove book completed " + book);
                if (repo.remove(book)) {
                    wasRemoving = true;
                }
            }
        }
        return wasRemoving;
    }

    @Override
    public boolean totalRemoving(Integer id, String author, String title, Integer size) {

        logger.info("Attempt to remove book: id: " + id + " author: "
                + author + " title: " + title + " size: " + size);
        String typeRemoving = "";

//      If user insert id then removing only one book:
        if (id != 0) {
            return removeItemById(id);
        }

//      Check type of removing:
        if (author.equals("")) {
            if (title.equals("")) {
                if (size != 0) {
                    typeRemoving = "bySize";
                }
            } else {
                typeRemoving = "byTitle";
            }
        } else {
            typeRemoving = "byAuthor";
        }

//      Run through the list of books:
        switch (typeRemoving) {
            case "byAuthor":
                return removeItemByAuthor(author);
            case "byTitle":
                return removeItemByTitle(title);
            case "bySize":
                return removeItemBySize(size);
            default:
                return false;
        }
    }

    @Override
    public List<Book> filter(Integer id, String author, String title, Integer size) {

        List<Book> filteredRepo = new ArrayList<>();

//        Search only one book by id
        if (id != 0) {
            for (Book book : repo) {
                if (book.getId().equals(id)) {
                    filteredRepo.add(book);
                }
            }
            return filteredRepo;
        }

        if (!(author.equals(""))) {
            for (Book book : repo) {
                if (book.getAuthor().equals(author)) {
                    filteredRepo.add(book);
                }
            }
            return filteredRepo;
        }

        if (!(title.equals(""))) {
            for (Book book : repo) {
                if (book.getTitle().equals(title)) {
                    filteredRepo.add(book);
                }
            }

            return filteredRepo;
        }

        if (size != 0) {
            for (Book book : repo) {
                if (book.getSize().equals(size)) {
                    filteredRepo.add(book);
                }
            }

            return filteredRepo;
        }

        return filteredRepo;
    }

}
