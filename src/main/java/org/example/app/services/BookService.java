package org.example.app.services;

import org.example.web.dto.Book;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.List;

@Service
public class BookService {

    private final BookProjectRepository<Book> bookRepo;

    @Autowired
    public BookService(BookProjectRepository<Book> bookRepo) {
        this.bookRepo = bookRepo;
    }

    public List<Book> getAllBooks() {
        return bookRepo.retrieveAll();
    }

    public void saveBook(Book book) {
        bookRepo.store(book);
    }

    public boolean totalRemoving(Integer id, String author, String title, Integer size) {

        return bookRepo.totalRemoving(id, author, title, size);
    }

    public List<Book> filter(Integer id, String author, String title, Integer size) {
        return bookRepo.filter(id, author, title, size);
    }
}
