package org.example.app.services;

import org.apache.log4j.Logger;
import org.example.web.dto.LoginForm;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;

@Repository
public class LoginRepository implements AccountProjectRepository<LoginForm>{

    private final Logger logger = Logger.getLogger(LoginRepository.class);
    private final List<LoginForm> repo = new ArrayList<>();

    @Override
    public List<LoginForm> retrieveAll() {

        return new ArrayList<>(repo);
    }

    @Override
    public void createAccount(LoginForm loginForm) {

        logger.info("Create account " + loginForm);
        repo.add(loginForm);
    }

    @Override
    public boolean correctAccount(String username, String password) {
        for(LoginForm account : retrieveAll()) {

            if(account.getUsername().equals(username) && account.getPassword().equals(password)) {
                return true;
            }
        }
        return false;
    }
}
