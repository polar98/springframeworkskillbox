package org.example.app.services;

import org.example.web.dto.LoginForm;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class LoginService {

    private final AccountProjectRepository<LoginForm> accountRepo;

    @Autowired
    public LoginService(AccountProjectRepository<LoginForm> accountRepo) {
        this.accountRepo = accountRepo;
    }

    public boolean authenticate(String username, String password) {

        return accountRepo.correctAccount(username, password);
    }

    public void createAccount(LoginForm loginForm) {
        accountRepo.createAccount(loginForm);
    }
}
