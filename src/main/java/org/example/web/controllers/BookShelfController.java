package org.example.web.controllers;

import org.apache.log4j.Logger;
import org.example.app.services.BookService;
import org.example.web.dto.Book;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

@Controller
@RequestMapping(value = "books")
public class BookShelfController {

    private Logger logger = Logger.getLogger(BookShelfController.class);
    private BookService bookService;

    @Autowired
    public BookShelfController(BookService bookService) {
        this.bookService = bookService;
    }

    @GetMapping("/shelf")
    public String books(Model model) {

        model.addAttribute("book", new Book());
        model.addAttribute("bookList", bookService.getAllBooks());
        return "book_shelf";
    }

    @PostMapping("/save")
    public String saveBook(Book book) {

        if (book.getAuthor().equals("") && book.getSize() == null &&
                book.getTitle().equals("")) {
            logger.info("Error: can't save empty book");
        } else {
            bookService.saveBook(book);
        }

        return "redirect:/books/shelf";
    }

    @PostMapping("/remove")
    public String removeBook(@RequestParam(value = "bookIdToRemove", defaultValue = "0") Integer bookIdToRemove,
                             @RequestParam(value = "bookAuthorToRemove", defaultValue = "") String bookAuthorToRemove,
                             @RequestParam(value = "bookTitleToRemove", defaultValue = "") String bookTitleToRemove,
                             @RequestParam(value = "bookSizeToRemove", defaultValue = "0") Integer bookSizeToRemove) {

        if (bookService.totalRemoving(bookIdToRemove, bookAuthorToRemove,
                bookTitleToRemove, bookSizeToRemove)) {
            logger.info("Success removing");
        } else {
            logger.info("Error: removing failed");
        }

        return "redirect:/books/shelf";
    }

    @PostMapping("/shelf/filter")
    public String filter(Model model,
                         @RequestParam(value = "id", defaultValue = "0") Integer id,
                         @RequestParam(value = "author", defaultValue = "") String author,
                         @RequestParam(value = "title", defaultValue = "") String title,
                         @RequestParam(value = "size", defaultValue = "0") Integer size) {

        model.addAttribute("filteredBookList", bookService.filter(id, author, title, size));

        return "filtered_book_shelf";
    }


}
