package org.example.web.controllers;

import org.apache.log4j.Logger;
import org.example.app.services.LoginRepository;
import org.example.app.services.LoginService;
import org.example.web.dto.LoginForm;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
@RequestMapping(value="/login")
public class LoginController {

    private Logger logger = Logger.getLogger(LoginController.class);
    private LoginService loginService;

    @Autowired
    public LoginController(LoginService loginService) {
        this.loginService = loginService;
    }

    @GetMapping
    public String login(Model model) {

        model.addAttribute("loginForm", new LoginForm());
        return "login_page";
    }

    @PostMapping("/auth")
    public String authenticate(@RequestParam(value = "username") String username,
                               @RequestParam(value = "password") String password) {

        if(loginService.authenticate(username, password)) {
            return "redirect:/books/shelf";
        } else {
            return "redirect:/login";
        }
    }

    @GetMapping("/register")
    public String register(Model model) {

        model.addAttribute("loginForm", new LoginForm());
        return "register_page";
    }

    @PostMapping("/register/create")
    public String createAccount(LoginForm loginForm) {

        loginService.createAccount(loginForm);
        return "redirect:/login";
    }
}
